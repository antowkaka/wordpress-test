<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'twitter' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';/m<b{&1e`):1>I=..dzog4<ryn&{pPYx;Qgul_kLaC:_6L:|5fYPq9z:&F_wfM3' );
define( 'SECURE_AUTH_KEY',  'nS^JX),3ouY]JJrdflhoTx<xay=,0MA&X;E3,P>oSNa$kAjaXkSm@hki]I(#$rlr' );
define( 'LOGGED_IN_KEY',    's!gZmpBFTgARtT3<P6`ZI,*Nxw)*E^ey*32(JwG2`7-]eY& <6l#o<qD9_!dql|g' );
define( 'NONCE_KEY',        'z2v;Vyt ao{*coKx5hG}${vl=[!:J2_f<rWJ>VgbatL23TdiW0+%:O&eUkPiB7<l' );
define( 'AUTH_SALT',        'C7>3=pRXKBtC9i0)}4a8-o%}iEc@;Mstv(@QPMYC2<}KoVpZaO|qThi,JZ5`$Pvn' );
define( 'SECURE_AUTH_SALT', '|~CXE6hI^6TLr$%G?,,{YHjH?.PD]&uWT)Dq28V^xq`I8WLHW0: uDqqcYl4A_>F' );
define( 'LOGGED_IN_SALT',   'r^$ifBxmyVqNoTgI$I=]g31Cef*H0]G|m&#z_bpb<vB;~;#Mwd9%W2BRso`tGa>M' );
define( 'NONCE_SALT',       'gC5/Vy]e9U/6<#7TtAo> _s,DH3J[l%+IgQyWT.xC$JLy>anbeN>e$5z+MK4h&;t' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
